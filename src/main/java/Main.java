import exceptions.MaximumNumberOfStudentsReached;
import org.Group;
import people.Person;
import people.Student;
import people.Trainer;
import util.DateGenerator;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        // Manually initialize 15 students; 4 groups and 3 trainers;
        List<Student> studentList = new ArrayList<>();
        int studentCount = 15;
        int yearIndex = 0;
        //dsaf
        boolean hasKnowledge = true;
        for (int i = 0; i < studentCount; i++) {
            studentList.add(
                    new Student(
                            "Deniss" + i,
                            "F" + i,
                            DateGenerator.getDate("11-03-200" + yearIndex),
                            hasKnowledge));

            yearIndex++;
            hasKnowledge = i % 2 == 0;
            if (yearIndex == 10) yearIndex = 0;
        }

        List<Trainer> trainers = new ArrayList<>();
        int trainersCount = 3;
        for (int i = 0; i < trainersCount; i++) {
            trainers.add(
                    new Trainer(
                            "Deniss" + i,
                            "F" + i,
                            DateGenerator.getDate("11-03-198" + i),
                            true));
        }
        List<Group> groups = new ArrayList<>();
        int groupCount = 4;
        int studentsFrom = 0;
        int studentsTo = 3;
        int studentGroupSize = 3;
        for (int i = 0; i < groupCount; i++) {
            Trainer trainer = i < trainers.size() ? trainers.get(i) : null;
            List<Student> studentsForGroup = new ArrayList<>(
                    studentList.subList(studentsFrom, studentsTo));
            Group newGroup = null;
            try {
                newGroup = new Group(
                        "Group" + i,
                        trainer,
                        studentsForGroup);
            } catch (MaximumNumberOfStudentsReached maximumNumberOfStudentsReached) {
                maximumNumberOfStudentsReached.printStackTrace();
                continue;
            }
            groups.add(newGroup);
            studentsFrom = studentsTo;
            studentsTo = studentsFrom + studentGroupSize;
            System.out.println(groups.get(groups.size() - 1));
        }


//        Collections.sort(groups.get(0).getStudentList(), new Comparator<Student>() {
//            @Override
//            public int compare(Student student, Student t1) {
//                return student.getLastName().compareTo(t1.getLastName());
//            }
//        });
//        System.out.println("_+_+_+_+");
//        System.out.println(groups.get(0).getStudentList());
//
//        groups.get(0).getStudentList()
//                .stream()
//                .sorted(Comparator.comparing(Person::getLastName).reversed())
//                .map(Person::getLastName)
//                .forEach(System.out::println);
//
//        try {
//            groups.get(1).addStudent(new Student("", "", new Date(), true));
//        } catch (MaximumNumberOfStudentsReached maximumNumberOfStudentsReached) {
//            maximumNumberOfStudentsReached.printStackTrace();
//        }
//
//        int max = Integer.MIN_VALUE;
//        Group biggestStudentCount = null;
//        for (int i = 0; i < groups.size(); i++) {
//            if (groups.get(i).getStudentList().size() > max) {
//                max = groups.get(i).getStudentList().size();
//                biggestStudentCount = groups.get(i);
//            }
//        }
//
//        System.out.println(biggestStudentCount);
//
//        for (Group group : groups) {
//            for (Student student : group.getStudentList()) {
//                LocalDate start = student.getDateOfBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//                LocalDate end = LocalDate.now(); // use for age-calculation: LocalDate.now()
//                long years = ChronoUnit.YEARS.between(start, end);
//                System.out.println(years);
//                if (years < 25) {
//                    System.out.println(student);
//                }
//            }
//        }
//
//
//        for (Group group : groups) {
//            if (group.getTrainer() != null) {
//
//                System.out.println(group.getTrainer().getLastName());
//            }
//
//            System.out.println(group.getTrainer() != null ? group.getTrainer().getLastName() : "");
//
//            group.getStudentList()
//                    .stream()
//                    .map(stud -> stud.getLastName())
//                    .forEach(lastName -> System.out.println(lastName));
//        }
//
//        for (Student student : studentList) {
//            if(student.isHasPreviousJavaKnowledge()) {
//                System.out.println(student);
//            }
//        }
//
//
//        max = Integer.MIN_VALUE;
//        biggestStudentCount = null;
//        for (int i = 0; i < groups.size(); i++) {
//
//            int counter = 0;
//            for (Student student : groups.get(i).getStudentList()) {
//                if(!student.isHasPreviousJavaKnowledge()) {
//                    counter++;
//                }
//            }
//
//            if (counter > max) {
//                max = counter;
//                biggestStudentCount = groups.get(i);
//            }
//        }
//
//        System.out.println(biggestStudentCount);


//        for (Group group : groups) {
//            for (Student student : group.getStudentList()) {
//                LocalDate start = student.getDateOfBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//                LocalDate end = LocalDate.now(); // use for age-calculation: LocalDate.now()
//                long years = ChronoUnit.YEARS.between(start, end);
//                System.out.println(years);
//                if (years < 20) {
//                    group.getStudentList().remove(student);
//                }
//            }
//        }

//        for (Group group : groups) {
//            for (int i = group.getStudentList().size() - 1; i >= 0; i--) {
//                Student student = group.getStudentList().get(i);
//                LocalDate start = student.getDateOfBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//                LocalDate end = LocalDate.now(); // use for age-calculation: LocalDate.now()
//                long years = ChronoUnit.YEARS.between(start, end);
//                System.out.println(years);
//                if (years < 20) {
//                    group.getStudentList().remove(student);
//                }
//            }
//        }

        System.out.println("sadfsadf");
        groups
                .stream()
                .forEach(group -> {
                    group.setStudentList(
                            group.getStudentList()
                                    .stream()
                                    .filter(student -> {
                                        LocalDate start = student.getDateOfBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                                        LocalDate end = LocalDate.now(); // use for age-calculation: LocalDate.now()
                                        long years = ChronoUnit.YEARS.between(start, end);
                                        return years >= 20;
                                    })
                                    .collect(Collectors.toList()));
                    System.out.println(group.getStudentList());
                });

    }
}
