package people;

import java.util.Date;

public class Trainer extends Person {
    private boolean isAuthorized;

    public Trainer(String firstName, String lastName, Date dateOfBirth, boolean isAuthorized) {
        super(firstName, lastName, dateOfBirth);
        this.isAuthorized = isAuthorized;
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "isAuthorized=" + isAuthorized +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}
