package org;

import exceptions.MaximumNumberOfStudentsReached;
import people.Student;
import people.Trainer;

import java.util.List;

public class Group {

    private String name;
    private Trainer trainer;
    private List<Student> studentList;

    public Group(String name, Trainer trainer, List<Student> studentList) throws MaximumNumberOfStudentsReached {
        this.name = name;
        this.trainer = trainer;
        if (studentList.size() > 5) {
            throw new MaximumNumberOfStudentsReached();
        }
        this.studentList = studentList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public void addStudent(Student student) throws MaximumNumberOfStudentsReached {
        if (studentList.size() > 5) {
            throw new MaximumNumberOfStudentsReached();
        }
        studentList.add(student);
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", trainer=" + trainer +
                ", studentList=" + studentList +
                '}';
    }
}
